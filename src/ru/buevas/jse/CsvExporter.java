package ru.buevas.jse;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import ru.buevas.jse.exception.CsvExportException;

public class CsvExporter {
    public void export(List<Object> content, String path) throws CsvExportException {
        StringBuilder stringBuilder = new StringBuilder();
        if (content.isEmpty()) {
            throw new IllegalArgumentException("Objects list is empty");
        }
        Class<?> sampleClass = content.get(0).getClass();
        if (!content.stream().allMatch(contentObject -> sampleClass.equals(contentObject.getClass()))) {
            throw new IllegalArgumentException("Different classes in list");
        }

        buildHeaders(stringBuilder, sampleClass);
        buildContent(stringBuilder, content);
        writeToFile(stringBuilder.toString(), path);
    }

    private void writeToFile(String content, String path) throws CsvExportException {
        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path))) {
            bos.write(content.getBytes());
        } catch (FileNotFoundException e) {
            throw new CsvExportException("File not found", e);
        } catch (IOException ex) {
            throw new CsvExportException("Error while write content to file", ex);
        }
    }

    private void buildHeaders(StringBuilder stringBuilder, Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            stringBuilder.append(field.getName() + ",");
        }
        stringBuilder.append(System.lineSeparator());
    }

   private void buildContent(StringBuilder stringBuilder, List<Object> content) {
        content.forEach(contentObject -> stringBuilder.append(contentObject.toString() + System.lineSeparator()));
   }
}
