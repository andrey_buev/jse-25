package ru.buevas.jse;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import ru.buevas.jse.exception.CsvExportException;
import ru.buevas.jse.model.Person;

public class Main {
    private static Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        List<Object> test = new ArrayList<>();
        Person ivan = new Person("Ivan",
                "Petrov",
                LocalDate.of(1990, 3, 1),
                "ivan@mail.ru");
        Person petr = new Person("Petr",
                "Ivanov",
                LocalDate.of(1993, 5, 21),
                "petr@yandex.ru");
        test.add(ivan);
        test.add(petr);

        CsvExporter csvExporter = new CsvExporter();
        try {
            csvExporter.export(test, "test.csv");
        } catch (IllegalArgumentException | CsvExportException e) {
            log.severe(e.getMessage());
        }
    }
}
