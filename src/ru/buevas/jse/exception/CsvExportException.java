package ru.buevas.jse.exception;

public class CsvExportException extends ExportException {

    public CsvExportException(String message) {
        super(message);
    }

    public CsvExportException(String message, Throwable cause) {
        super(message, cause);
    }
}
